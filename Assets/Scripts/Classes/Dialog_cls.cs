﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialog_cls
{
    public string Dialog;
    public string CanSpeak;
}